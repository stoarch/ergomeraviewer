package com.efl_metering.ergomeraview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.efl_metering.ergomeraview.MeterEventFragment.OnListFragmentInteractionListener;
import com.efl_metering.ergomeraview.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyMeterEventRecyclerViewAdapter extends RecyclerView.Adapter<MyMeterEventRecyclerViewAdapter.ViewHolder> {

    private final List<MeterEvent> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyMeterEventRecyclerViewAdapter(List<MeterEvent> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_meterevent, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(String.valueOf(position));
        holder.mContentView.setText(mValues.get(position).getContent());
        holder.mCatView.setText(String.valueOf(mValues.get(position).getCat()));
        holder.mCodeView.setText(String.valueOf(mValues.get(position).getCode()));
        holder.mUpView.setText(String.valueOf(mValues.get(position).getUp()));
        holder.mValView.setText(String.valueOf(mValues.get(position).getVal()));
        holder.mTimeView.setText(String.valueOf(mValues.get(position).getTime()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if( mValues != null )
            return mValues.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mCodeView;
        public final TextView mCatView;
        public final TextView mUpView;
        public final TextView mValView;
        public final TextView mTimeView;
        public MeterEvent mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            mCodeView = (TextView) view.findViewById(R.id.code);
            mCatView = (TextView) view.findViewById(R.id.cat);
            mUpView = (TextView) view.findViewById(R.id.up);
            mValView = (TextView) view.findViewById(R.id.val);
            mTimeView = (TextView) view.findViewById(R.id.time);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
