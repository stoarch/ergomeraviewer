package com.efl_metering.ergomeraview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kipserver on 09.04.2016.
 */
public class EventListJSONParser {
    private ArrayList<MeterEvent> eventList;

    public void parseEventList(String response)
    {
        try {
            JSONObject root = new JSONObject(response);
            eventList = new ArrayList<>();

            JSONArray jsonValues = root.getJSONArray("events");

            for (int i = 0; i < jsonValues.length(); i++ ){
                JSONObject jsonValInfo = jsonValues.getJSONObject(i);
                JSONObject jsonValAttribs = jsonValInfo.getJSONObject("attributes");

                eventList.add(
                        new MeterEvent()
                                .setContent(jsonValInfo.getString("content"))
                                .setCode(jsonValAttribs.getInt("code"))
                                .setCat(jsonValAttribs.getInt("cat"))
                                .setUp(jsonValAttribs.getInt("up"))
                                .setVal(jsonValAttribs.getInt("val"))
                                .setTime(jsonValAttribs.getString("time"))
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MeterEvent> getEventList() {
        return eventList;
    }
}
