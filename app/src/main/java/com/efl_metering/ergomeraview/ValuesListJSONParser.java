package com.efl_metering.ergomeraview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kipserver on 08.04.2016.
 */
public class ValuesListJSONParser {
    private ArrayList<MeterValue> valueList;
    public ArrayList<MeterValue> getValueList(){
        return valueList;
    }

    public void parseValueList(String response) {
        try {
            JSONObject root = new JSONObject(response);
            valueList = new ArrayList<>();

            JSONArray jsonValues = root.getJSONArray("values");

            for (int i = 0; i < jsonValues.length(); i++ ){
                JSONObject jsonValInfo = jsonValues.getJSONObject(i);
                JSONObject jsonValAttribs = jsonValInfo.getJSONObject("attributes");

                valueList.add(
                        new MeterValue()
                                .setValue(jsonValInfo.getDouble("content"))
                                .setId(jsonValAttribs.getInt("id"))
                                .setTime(jsonValAttribs.getString("time"))
                                .setLogTime(jsonValAttribs.getString("log_time"))
                                .setTarif(jsonValAttribs.getInt("tarif"))
                                .setStatus(jsonValAttribs.getInt("status"))
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

