package com.efl_metering.ergomeraview;

/**
 * Created by kipserver on 08.04.2016.
 */
public class MeterValue {
    private double value;
    private int id;
    private String time;
    private int tarif;
    private String logTime;
    private int status;

    public double getValue(){ return value; }
    public int getId(){ return id; }
    public String getTime(){ return time; }
    public int getTarif(){ return tarif; }
    public String getLogTime(){ return logTime; }
    public int getStatus(){ return status; }

    public MeterValue setValue( double avalue ){ value = avalue; return this; }
    public MeterValue setId( int aid ){ id = aid; return this; }
    public MeterValue setTime( String atime ){ time = atime; return this; }
    public MeterValue setLogTime( String atime ){ logTime = atime; return this; }
    public MeterValue setTarif( int atarif ){ tarif = atarif; return this; }
    public MeterValue setStatus( int astatus ){ status = astatus; return this; }
}
