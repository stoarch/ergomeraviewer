package com.efl_metering.ergomeraview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.efl_metering.ergomeraview.MeterCurrentValueFragment.OnListFragmentInteractionListener;
import com.efl_metering.ergomeraview.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyMeterCurrentValueRecyclerViewAdapter extends RecyclerView.Adapter<MyMeterCurrentValueRecyclerViewAdapter.ViewHolder> {

    private final List<MeterValue> mValues;
    private final List<MeterParam> mParams;
    private final OnListFragmentInteractionListener mListener;

    public MyMeterCurrentValueRecyclerViewAdapter(List<MeterValue> items, List<MeterParam> params,  OnListFragmentInteractionListener listener) {
        mValues = items;
        mParams = params;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_metercurrentvalue, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if( mParams.size() == 0 )
            return;

        holder.mItem = mValues.get(position);
        holder.mIdView.setText(String.valueOf(position));
        holder.mValueView.setText(String.valueOf(mValues.get(position).getValue()));
        holder.mStatusView.setText(String.valueOf(mValues.get(position).getStatus()));
        holder.mTarifView.setText(String.valueOf(mValues.get(position).getTarif()));
        holder.mTimeView.setText(String.valueOf(mValues.get(position).getTime()));
        holder.mLogTimeView.setText(String.valueOf(mValues.get(position).getLogTime()));

        holder.mParamCaptionView.setText(mParams.get(position).getCaption());
        holder.mParamNameView.setText(mParams.get(position).getName());
        holder.mParamUnitsView.setText(mParams.get(position).getUnits());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(String.valueOf(holder.mItem.getId()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if( mValues != null )
            return mValues.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mValueView;
        public final TextView mStatusView;
        public final TextView mTarifView;
        public final TextView mTimeView;
        public final TextView mLogTimeView;
        public final TextView mParamCaptionView;
        public final TextView mParamNameView;
        public final TextView mParamUnitsView;
        public MeterValue mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mValueView = (TextView) view.findViewById(R.id.value);
            mStatusView = (TextView) view.findViewById(R.id.status);
            mTarifView = (TextView) view.findViewById(R.id.tarif);
            mTimeView = (TextView) view.findViewById(R.id.time);
            mLogTimeView = (TextView) view.findViewById(R.id.log_time);

            mParamCaptionView = (TextView) view.findViewById(R.id.param_caption);
            mParamNameView = (TextView) view.findViewById(R.id.param_name);
            mParamUnitsView = (TextView) view.findViewById(R.id.param_units);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mIdView.getText() + "'";
        }
    }
}
