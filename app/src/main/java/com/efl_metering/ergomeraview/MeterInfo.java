package com.efl_metering.ergomeraview;

import java.io.Serializable;

/**
 * Created by kipserver on 24.03.2016.
 */
class MeterInfo implements Serializable{
    private String caption;
    private int id;
    private int masterId;
    private String typeName;
    private String objectName;
    private String location;
    private String displayName;

    //FLUENT INTERFACE for Builder pattern//
    public MeterInfo setCaption(String acaption) {
        caption = acaption;
        return this;
    }

    public String getCaption(){ return caption; }
    public int getId(){ return id; }
    public int getMasterId(){ return masterId;}
    public String getTypeName(){ return typeName; }
    public String getObjectName(){ return objectName; }
    public String getLocation(){ return  location; }
    public String getDisplayName(){ return displayName; }

    public MeterInfo setId(int aid) {
        id = aid;
        return this;
    }

    public MeterInfo setMasterId(int amasterId) {
        masterId = amasterId;
        return this;
    }

    public MeterInfo setTypeName(String atypeName) {
        typeName = atypeName;
        return this;
    }

    public MeterInfo setObjectName(String aobjectName) {
        objectName = aobjectName;
        return this;
    }

    public MeterInfo setLocation(String alocation) {
        location = alocation;
        return this;
    }

    public MeterInfo setDisplayName(String adisplayName) {
        displayName = adisplayName;
        return this;
    }
}
