package com.efl_metering.ergomeraview;

/**
 * Created by kipserver on 09.04.2016.
 */
public class MeterParam {
    private String caption;
    private String name;
    private String units;
    private Integer id;

    public MeterParam setCaption(String value){ caption = value; return this;}
    public MeterParam setName(String value){ name =  value; return this;}
    public MeterParam setUnits(String value){ units = value; return this;}

    public String getCaption() {
        return caption;
    }

    public String getUnits() {
        return units;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public MeterParam setId(Integer id) {
        this.id = id;
        return this;
    }
}
