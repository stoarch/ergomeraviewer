package com.efl_metering.ergomeraview;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewDebug;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.methods.HttpGet;

import java.net.URI;
import java.util.ArrayList;

public class ChildMetersViewActivity extends AppCompatActivity {
    ArrayList<String> meters = new ArrayList<>();

    private static final String LOAD_ACTION = "com.efl_metering.ergomeraview";
    private ProgressDialog progress;

    private MeterListResult metersResult;
    private MeterListJSONParser resultParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_meters_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        resultParser = new MeterListJSONParser();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MeterInfo meter = (MeterInfo)getIntent().getSerializableExtra("meter");

        TextView captionText = (TextView)findViewById(R.id.submeter_caption);
        assert captionText != null;
        captionText.setText(getString(R.string.meter_caption) + meter.getCaption());
        setTitle(getString(R.string.subgroup_caption) + meter.getCaption());

        ListView metersListView = (ListView) findViewById(R.id.submeters_list);

        metersListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(getBaseContext(),
                                "Selected:" + meters.get(position),
                                Toast.LENGTH_SHORT)
                                .show();

                        MeterInfo meter = metersResult.getMeter(position);

                        //call self on deeper hierarchy level
                        Intent subMetersIntent = new Intent(ChildMetersViewActivity.this, ChildMetersViewActivity.class);
                        subMetersIntent.putExtra("id", meter.getId());
                        subMetersIntent.putExtra("meter", meter);
                        startActivity(subMetersIntent);
                    }
                }
        );

        GetSubChilds(meter.getId());
    }

    private void GetSubChilds(int meterId) {

        //Get data//
        try {
            String url = "http://92.47.27.114:9291/info/devices/roots/" + Integer.toString( meterId ) + "/childs";
            HttpGet searchRequest = new HttpGet(new URI(url));

            RestTask task = new RestTask(this, LOAD_ACTION);
            task.execute(searchRequest);
            progress = ProgressDialog.show(this, "Loading", "Wait for meters...", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(LOAD_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }


    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (progress != null) {
                progress.dismiss();
            }

            String response = intent.getStringExtra(RestTask.HTTP_RESPONSE);
            resultParser.parseMeterList(response);

            metersResult = resultParser.getMeterList();
            meters = resultParser.getMetersCaption();

            ListView metersListView = (ListView) findViewById(R.id.submeters_list);
            ArrayAdapter<String> metersList = new ArrayAdapter<String>(context,
                    android.R.layout.simple_list_item_1,
                    meters
            );
            metersListView.setAdapter(metersList);
        }
    };

}
