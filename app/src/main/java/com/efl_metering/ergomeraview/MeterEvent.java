package com.efl_metering.ergomeraview;

/**
 * Created by kipserver on 09.04.2016.
 */
public class MeterEvent {
    private String content;
    private int code;
    private int cat;
    private int up;
    private int val;
    private String time;

    public int getCat() {
        return cat;
    }

    public int getCode() {
        return code;
    }

    public int getUp() {
        return up;
    }

    public int getVal() {
        return val;
    }

    public String getContent() {
        return content;
    }

    public String getTime() {
        return time;
    }

    public MeterEvent setCat(int cat) {
        this.cat = cat;
        return this;
    }

    public MeterEvent setCode(int code) {
        this.code = code;
        return this;
    }

    public MeterEvent setContent(String content) {
        this.content = content;
        return  this;
    }

    public MeterEvent setTime(String time) {
        this.time = time;
        return  this;
    }

    public MeterEvent setUp(int up) {
        this.up = up;
        return  this;
    }

    public MeterEvent setVal(int val) {
        this.val = val;
        return  this;
    }
}
