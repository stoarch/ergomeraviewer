package com.efl_metering.ergomeraview;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kipserver on 25.03.2016.
 */
public class MeterListResult {
    private int returnCode = -1;
    private ArrayList<MeterInfo> meters;

    MeterListResult(){
        meters = new ArrayList<MeterInfo>();
    }

    public void setReturnCode( int code )
    {
        returnCode = code;
    }

    public void addMeterInfo( MeterInfo newInfo )
    {
        meters.add( newInfo );
    }

    public MeterInfo getMeter( int index )
    {
        return meters.get(index);
    }
    public int getCount(){ return meters.size();}

    public ArrayList<MeterInfo> getMeters(){ return meters; }

}
