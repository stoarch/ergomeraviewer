package com.efl_metering.ergomeraview;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.efl_metering.ergomeraview.dummy.DummyContent;

import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Intent.getIntent;
import static android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE;

public class SubnodesContainer extends AppCompatActivity
        implements
            SubMeterFragment.OnListFragmentInteractionListener,
            MeterCurrentValueFragment.OnListFragmentInteractionListener,
            MeterEventFragment.OnListFragmentInteractionListener,
            DatePickerDialog.OnDateSetListener

{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private int meterId;
    private MeterInfo meter;
    private final SubMeterFragment subMeters = new SubMeterFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        meter = (MeterInfo)getIntent().getSerializableExtra("meter");
        if( meter != null ) {
            meterId = meter.getId();
            setTitle(meter.getCaption());
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subnodes_container);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        subMeters.setOnMetersLoaded(
                new SubMeterFragment.OnMetersLoaded() {
                    @Override
                    public void onLoaded() {
                        invalidateOptionsMenu();
                    }
                }
        );
    }

    @Override
    public boolean onPrepareOptionsMenu( Menu menu )
    {
        MenuItem hourItem = menu.findItem(R.id.action_day_archive);
        MenuItem monthItem = menu.findItem(R.id.action_month_archive);

        if(subMeters.meters.size() == 0) {
            hourItem.setEnabled(true);
            monthItem.setEnabled(true);
        }
        else
        {
            if( subMeters.meters.get(0) == "Нет подсчётчиков") {
                hourItem.setEnabled(true);
                monthItem.setEnabled(true);
            }
            else {
                hourItem.setEnabled(false);
                monthItem.setEnabled(false);
            }
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subnodes_container, menu);
        return true;
    }
    private static final int MONTH_ARCHIVE = 1;
    private static final int DAY_ARCHIVE = 2;

    private int activeArchive;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_month_archive)
        {
            activeArchive = MONTH_ARCHIVE;
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }
        else if(id == R.id.action_day_archive)
        {
            activeArchive = DAY_ARCHIVE;
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(String item) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        Intent intent = null;
        if( activeArchive == MONTH_ARCHIVE ) {
            intent = new Intent(this, MonthArchiveViewActivity.class);
        }

        if( intent != null) {
            intent.putExtra("meter", meter);
            intent.putExtra("year_start", year);
            intent.putExtra("month_start", monthOfYear);
            intent.putExtra("day_start", dayOfMonth);
            intent.putExtra("year_end", yearEnd);
            intent.putExtra("month_end", monthOfYearEnd);
            intent.putExtra("day_end", dayOfMonthEnd);
            startActivity(intent);
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_subnodes_container, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<Fragment> mFragments;

        public SectionsPagerAdapter(FragmentManager fm)
        {
            super(fm);
            Bundle args = new Bundle();
            args.putInt("meterId", meterId);
            args.putSerializable("meter", meter);

            mFragments = new ArrayList<Fragment>();
            subMeters.setArguments(args);
            mFragments.add(subMeters);

            //Values
            MeterCurrentValueFragment curValFragment = new MeterCurrentValueFragment();
            curValFragment.setArguments(args);
            mFragments.add(curValFragment);

            MeterEventFragment eventFragment = new MeterEventFragment();
            eventFragment.setArguments(args);
            mFragments.add( eventFragment );
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Счётчики";
                case 1:
                    return "Значения";
                case 2:
                    return "События";
            }
            return null;
        }
    }
}
