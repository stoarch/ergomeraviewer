package com.efl_metering.ergomeraview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kipserver on 13.04.2016.
 */
public class MeterArchiveJSONParser {
    private ArrayList<MeterArchiveItem> meterArchive;

    public ArrayList<MeterArchiveItem> getMeterArchive(){
        return meterArchive;
    }

    public void parseMeterArchive(String response) {
        try {
            JSONObject root = new JSONObject(response);
            meterArchive = new ArrayList<>();

            JSONArray jsonMeters = root.getJSONArray("params");

            for (int i = 0; i < jsonMeters.length(); i++ ){
                JSONObject jsonArchiveItem = jsonMeters.getJSONObject(i);
                JSONObject jsonAttribs = jsonArchiveItem.getJSONObject("attributes");

                meterArchive.add(
                        new MeterArchiveItem()
                                .setValue(jsonArchiveItem.getDouble("content"))
                                .setParamId(jsonAttribs.getInt("param_id"))
                                .setKind(jsonAttribs.getInt("kind"))
                                .setRange(jsonAttribs.getInt("range"))
                                .setTimeMark(jsonAttribs.getString("time_mark"))
                                .setTime(jsonAttribs.getString("time"))
                                .setLogTime(jsonAttribs.getString("log_time"))
                                .setRange(jsonAttribs.getInt("tarif"))
                                .setStatus(jsonAttribs.getInt("status"))
                );
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

