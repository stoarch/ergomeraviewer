package com.efl_metering.ergomeraview;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> meters = new ArrayList<>();

    private static final String LOAD_ACTION = "com.efl_metering.ergomeraview";
    private ProgressDialog progress;
    private MeterListJSONParser resultParser;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Главные группы (Шымкент)");

        resultParser = new MeterListJSONParser();

        ListView metersListView = (ListView) findViewById(R.id.energia_meter_list);
        final ArrayAdapter<String> metersListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, meters);
        metersListView.setAdapter(metersListAdapter);

        metersListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(meterList.getCount() > 0 ) {
                            MeterInfo meter = meterList.getMeter(position);
                            Intent subNodesIntent = new Intent(MainActivity.this, SubnodesContainer.class);
                            subNodesIntent.putExtra("meter", meter);
                            startActivity(subNodesIntent);
                        }
                        else
                        {
                            Toast.makeText(MainActivity.this, "No meters found", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
        GetRootChilds();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(meterList.getCount() > 0 ) {
                    MeterInfo meter = meterList.getMeter(0);
                    Intent subNodesIntent = new Intent(MainActivity.this, SubnodesContainer.class);
                    subNodesIntent.putExtra("meter", meter);
                    startActivity(subNodesIntent);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "No meters found", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void GetRootChilds() {
        if( meterList != null )
            return;

        //Get data//
        try {
            String url = "http://92.47.27.114:9291/info/devices/roots/1171/childs";
            HttpGet searchRequest = new HttpGet(new URI(url));

            RestTask task = new RestTask(this, LOAD_ACTION);
            task.execute(searchRequest);
            progress = ProgressDialog.show(this, "Loading", "Wait for meters...", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(LOAD_ACTION));
    }

    @Override
    public void onPause() {
        if (progress != null) {
            progress.dismiss();
        }
        super.onPause();
        unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (progress != null) {
                progress.dismiss();
                progress = null;
            }

            String response = intent.getStringExtra(RestTask.HTTP_RESPONSE);
            if( response != null ) {
                resultParser.parseMeterList(response);

                meterList = resultParser.getMeterList();
                meters = resultParser.getMetersCaption();

                ListView metersListView = (ListView) findViewById(R.id.energia_meter_list);
                ArrayAdapter<String> metersList = new ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_list_item_1,
                        meters
                );
                metersListView.setAdapter(metersList);
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.efl_metering.ergomeraview/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.efl_metering.ergomeraview/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


    private MeterListResult meterList = null;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
