package com.efl_metering.ergomeraview;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kipserver on 16.04.2016.
 */
public class MeterParams {
    private ArrayList<MeterParam> params;
    private HashMap<Integer, MeterParam> paramById;
    public MeterParams()
    {
        params = new ArrayList<>();
        paramById = new HashMap<>();
    }

    public void add(MeterParam param)
    {
        params.add(param);
        paramById.put(param.getId(), param);
    }

    public MeterParam findById( int id )
    {
        return paramById.get(id);
    }

    public void loadFrom(ArrayList<MeterParam> paramList) {
        for (int i = 0; i < paramList.size(); i++ )
        {
            add(paramList.get(i));
        }
    }
}
