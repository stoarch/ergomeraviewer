package com.efl_metering.ergomeraview;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efl_metering.ergomeraview.dummy.DummyContent;
import com.efl_metering.ergomeraview.dummy.DummyContent.DummyItem;

import org.apache.http.client.methods.HttpGet;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MeterEventFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    
    private ArrayList<MeterEvent> events;
    private RecyclerView listView;
    private int meterId;
    private EventListJSONParser resultParser;

    private Context mContext;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MeterEventFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meterevent_list, container, false);
        mContext = getActivity().getBaseContext();

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyMeterEventRecyclerViewAdapter(events, mListener));
            listView = recyclerView;
            meterId= getArguments().getInt("meterId");
            LoadEvents();
        }
        return view;
    }

    private static final String LOAD_ACTION = "com.efl_metering.ergomeraview";

    private void LoadEvents() {
        new Thread(){
            public void run(){
                //Get data//
                try {
                    String url = "http://92.47.27.114:9291/info/events/" + Integer.toString( meterId );
                    HttpGet searchRequest = new HttpGet(new URI(url));

                    RestTask task = new RestTask(mContext.getApplicationContext(), LOAD_ACTION);
                    String response = task.executeSync(mContext, url);

                    resultParser = new EventListJSONParser();

                    resultParser.parseEventList(response);
                    events = resultParser.getEventList();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.setAdapter(new MyMeterEventRecyclerViewAdapter(events, mListener));
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(String item);
    }
}
