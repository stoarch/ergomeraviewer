package com.efl_metering.ergomeraview;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by kipserver on 19.03.2016.
 */
public class RestTask extends AsyncTask<HttpUriRequest, Void, String> {
    public static final String HTTP_RESPONSE = "httpResponse";

    private Context mContext;
    private HttpClient mClient;
    private String mAction;

    public RestTask(Context context, String action){
        mContext = context;
        mAction = action;
        mClient = new DefaultHttpClient();
    }

    public RestTask(Context context, String action, HttpClient client){
        mContext = context;
        mClient = client;
        mAction = action;
    }

    public String executeSync(Context context, String url)
    {
        String result = "";
        try {
            DownloadTextTask downloader = new DownloadTextTask(context);
            result = downloader.execute(url).get();
        }
        catch(Exception e)
        {
            Log.e("RestTask::>> ", e.getMessage());
        }
        return result;
    }

    private String getToServer( String service ) throws Exception {
        URL url = new URL(service);
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        String res = "";
        try{
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while((line = reader.readLine()) != null)
            {
                buffer.append(line);
            }
            res = buffer.toString();
        }
        finally {

            urlConnection.disconnect();
        }
        return res;
    }

    String urlText = "";

    private class DownloadTextTask extends AsyncTask<String, Void, String> {
        private final Context context;
        public DownloadTextTask(Context contextParm)
        {
            context = contextParm;
        }
        @Override
        protected String doInBackground(String... urls) {
            try {
                return getToServer(urls[0]);
            }catch (Exception e){
                Log.e("PutReceiver:>>", e.getLocalizedMessage());
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result){
            urlText = result;
        }

    }
    @Override
    protected String doInBackground(HttpUriRequest... params){
        try{
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
            HttpUriRequest request = params[0];
            HttpResponse serverResponse = mClient.execute(request);

            BasicResponseHandler handler = new BasicResponseHandler();
            String response = handler.handleResponse(serverResponse);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result){
        Intent intent = new Intent(mAction);
        intent.putExtra(HTTP_RESPONSE, result);
        mContext.sendBroadcast(intent);
    }

}
