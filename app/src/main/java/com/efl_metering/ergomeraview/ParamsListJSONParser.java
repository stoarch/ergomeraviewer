package com.efl_metering.ergomeraview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kipserver on 09.04.2016.
 */
public class ParamsListJSONParser {
    private ArrayList<MeterParam> paramList;
    public ArrayList<MeterParam> getParamList(){
        return paramList;
    }

    public void parseParamList(String response) {
        try {
            JSONObject root = new JSONObject(response);
            paramList = new ArrayList<>();

            JSONArray jsonValues = root.getJSONArray("params");

            for (int i = 0; i < jsonValues.length(); i++ ){
                JSONObject jsonValInfo = jsonValues.getJSONObject(i);
                JSONObject jsonValAttribs = jsonValInfo.getJSONObject("attributes");

                paramList.add(
                        new MeterParam()
                                .setCaption(jsonValInfo.getString("content"))
                                .setId(jsonValAttribs.getInt("param_id"))
                                .setName(jsonValAttribs.getString("name"))
                                .setUnits(jsonValAttribs.getString("units"))
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
