package com.efl_metering.ergomeraview;

import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by kipserver on 25.03.2016.
 */
public class MeterListJSONParser {
    private MeterListResult meterList;
    public MeterListResult getMeterList(){
        return meterList;
    }

    public ArrayList<String> getMetersCaption(){
        ArrayList<String> meters = new ArrayList<>(meterList.getCount());
        for ( int i = 0; i < meterList.getCount(); i++){
            meters.add(meterList.getMeter(i).getCaption());
        }
        return meters;
    }

    public void parseMeterList(String response) {
        try {
            JSONObject root = new JSONObject(response);
            meterList = new MeterListResult();
            meterList.setReturnCode(root.getInt("return"));

            JSONArray jsonMeters = root.getJSONArray("devices");

            for (int i = 0; i < jsonMeters.length(); i++ ){
                JSONObject jsonMeterInfo = jsonMeters.getJSONObject(i);
                JSONObject jsonMeterAttribs = jsonMeterInfo.getJSONObject("attributes");

                meterList.addMeterInfo(
                        new MeterInfo().setCaption(jsonMeterInfo.getString("content"))
                                .setId(jsonMeterAttribs.getInt("id"))
                                .setMasterId(jsonMeterAttribs.getInt("master_id"))
                                .setTypeName(jsonMeterAttribs.getString("type_name"))
                                .setLocation(jsonMeterAttribs.getString("location"))
                                .setDisplayName(jsonMeterAttribs.getString("display_name"))
                );
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
