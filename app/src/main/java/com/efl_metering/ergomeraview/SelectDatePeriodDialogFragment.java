package com.efl_metering.ergomeraview;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SelectDatePeriodDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SelectDatePeriodDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectDatePeriodDialogFragment extends DialogFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String DATE_START = "date_start";
    private static final String DATE_END = "date_end";

    // TODO: Rename and change types of parameters
    private String dateStart;
    private String dateEnd;

    private OnFragmentInteractionListener mListener;

    public SelectDatePeriodDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SelectDatePeriodDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelectDatePeriodDialogFragment newInstance(String adateStart, String adateEnd) {
        SelectDatePeriodDialogFragment fragment = new SelectDatePeriodDialogFragment();
        Bundle args = new Bundle();
        args.putString(DATE_START, adateStart);
        args.putString(DATE_END, adateEnd);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dateStart = getArguments().getString(DATE_START);
            dateEnd = getArguments().getString(DATE_END);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       getDialog().setTitle("Выберите диаппазон дат");
        // Inflate the layout for this fragment
       View v = inflater.inflate(R.layout.fragment_select_date_period_dialog, container, false);
        v.findViewById(R.id.commit_button).setOnClickListener(this);
        v.findViewById(R.id.discard_button).setOnClickListener(this);

        return v;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
