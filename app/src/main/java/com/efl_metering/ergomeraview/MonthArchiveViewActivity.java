package com.efl_metering.ergomeraview;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.apache.http.client.methods.HttpGet;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MonthArchiveViewActivity extends AppCompatActivity {

    private int meterId;
    private MeterInfo meter;
    private ArrayList<MeterArchiveItem> meterArchive;
    private ListView listView;
    private ProgressDialog progress;
    private LineChart chart;
    private Spinner paramSpinner;
    private int yearStart, monthStart, dayStart;
    private int yearEnd, monthEnd, dayEnd;
    private ParamsListJSONParser paramParser;
    private MeterParams params ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_archive_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        chart = (LineChart) findViewById(R.id.chart);
        chart.setDrawBorders(true);

        params = new MeterParams();

        paramSpinner = (Spinner) findViewById(R.id.param_spinner);
        List<String> vals = Arrays.asList("Param 1", "Param 2", "Param 3");
        ArrayAdapter<String> paramKindAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, vals);
        paramSpinner.setAdapter(paramKindAdapter);

        listView = (ListView)findViewById(R.id.month_archive_list);
        progress = ProgressDialog.show(this, "Loading", "Wait for archive...", true);

        Intent pi = getIntent();

        yearStart = pi.getIntExtra("year_start",0);
        yearEnd = pi.getIntExtra("year_end",0);
        monthStart = pi.getIntExtra("month_start",0);
        monthEnd = pi.getIntExtra("month_end",0);
        dayStart = pi.getIntExtra("day_start",0);
        dayEnd = pi.getIntExtra("day_end",0);

        meter = (MeterInfo)pi.getSerializableExtra("meter");
        if( meter != null ) {
            meterId = meter.getId();
            setTitle("Месячный архив: " + meter.getCaption());
        }
        else {

            setTitle("Месячный архив расходомера №хххх");
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getArchives(meterId);
    }

    private void fillChart() {
        ArrayList<Entry> vals = new ArrayList<>();
        for (int i = 0; i < meterArchive.size(); i++ )
        {
            Entry e1 = new Entry((float)meterArchive.get(i).getValue(),i);
            vals.add(e1);
        }


        LineDataSet setVals = new LineDataSet(vals, String.valueOf(meterArchive.get(0).getParamId()));
        setVals.setAxisDependency(YAxis.AxisDependency.LEFT);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(setVals);

        ArrayList<String> xVals = new ArrayList<>();
        for (int i = 0; i < meterArchive.size(); i++) {
            xVals.add(meterArchive.get(i).getTime());
        }

        LineData data = new LineData( xVals, dataSets );
        chart.setData(data);
        chart.invalidate();
    }

    final static int HOUR_ARCHIVE = 1;
    final static int DAY_ARCHIVE = 2;
    final static int MONTH_ARCHIVE = 3;

    private static final String LOAD_ACTION = "com.efl_metering.ergomeraview";

    private void getArchives(final int meterId) {

        new Thread(){
            public void run(){
                //Get data//
                try {
                    String url = "http://92.47.27.114:9291/devices/" + Integer.toString( meterId ) + String.format("/params/0/archives/month/'%d.%d.%d'/'%d.%d.%d'", dayStart, monthStart, yearStart, dayEnd, monthEnd, yearEnd);
                    HttpGet searchRequest = new HttpGet(new URI(url));

                    RestTask task = new RestTask(getBaseContext(), LOAD_ACTION);
                    String response = task.executeSync(getBaseContext(), url);

                    MeterArchiveJSONParser resultParser = new MeterArchiveJSONParser();

                    resultParser.parseMeterArchive(response);
                    meterArchive = resultParser.getMeterArchive();

                    url = "http://92.47.27.114:9291/info/devices/" + Integer.toString( meterId ) + "/params";
                    searchRequest = new HttpGet(new URI(url));

                    task = new RestTask(MonthArchiveViewActivity.this, LOAD_ACTION);
                    response = task.executeSync(MonthArchiveViewActivity.this, url);

                    paramParser = new ParamsListJSONParser();

                    paramParser.parseParamList(response);
                    ArrayList<MeterParam> paramList = paramParser.getParamList();
                    params.loadFrom(paramList);


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fillChart();
                            listView.setAdapter(new MonthArchiveListAdapter(MonthArchiveViewActivity.this, meterArchive, params));
                            if (progress != null)
                                progress.dismiss();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

}
