package com.efl_metering.ergomeraview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by kipserver on 13.04.2016.
 */
public class MonthArchiveListAdapter extends  ArrayAdapter<MeterArchiveItem>{

    private final Activity context;
    private final ArrayList<MeterArchiveItem> meterArchive;
    private final MeterParams params;

    public MonthArchiveListAdapter(Activity acontext, ArrayList<MeterArchiveItem> ameterArchive, MeterParams params) {
        super(acontext, R.layout.month_archive_list_item_layout, ameterArchive );
        this.context = acontext;
        this.meterArchive = ameterArchive;
        this.params = params;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        MeterArchiveItem item = meterArchive.get(position);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView;
        if( position == 0 ) {
            rowView = inflater.inflate(R.layout.month_archive_list_item_with_header_layout, null, true);
            TextView paramCaptionView = (TextView)rowView.findViewById(R.id.param_caption);
            MeterParam param = params.findById( item.getParamId() );
            if( param != null)
                paramCaptionView.setText( param.getCaption() );
            else
                paramCaptionView.setText( "Не известно" );
        }
        else {
            MeterArchiveItem prevItem = meterArchive.get(position - 1);
            if( prevItem.getParamId() != item.getParamId() ) {
                rowView = inflater.inflate(R.layout.month_archive_list_item_with_header_layout, null, true);
                TextView paramCaptionView = (TextView)rowView.findViewById(R.id.param_caption);
                MeterParam param = params.findById( item.getParamId() );
                if( param != null)
                    paramCaptionView.setText( param.getCaption() );
                else
                    paramCaptionView.setText( "Не известно" );
            }
            else
                rowView = inflater.inflate(R.layout.month_archive_list_item_layout, null, true);
        }

        TextView idText = (TextView) rowView.findViewById(R.id.month_archive_id);
        TextView kindText = (TextView) rowView.findViewById(R.id.month_archive_kind);
        TextView tarifText = (TextView) rowView.findViewById(R.id.month_archive_tarif);
        TextView valueText = (TextView) rowView.findViewById(R.id.month_archive_value);
        TextView statusText = (TextView) rowView.findViewById(R.id.month_archive_status);
        TextView timeMarkText = (TextView) rowView.findViewById(R.id.month_archive_time_mark);
        TextView timeText = (TextView) rowView.findViewById(R.id.month_archive_time);
        TextView timeLogText = (TextView) rowView.findViewById(R.id.month_archive_log_time);


        idText.setText(String.valueOf(position));
        kindText.setText((item.getKind() == 1) ? ("Интервал") : ((item.getKind() == 2) ? ("Итог") : "???"));
        tarifText.setText(String.valueOf(item.getTarif()));
        statusText.setText(getStatusText(item.getStatus()));
        valueText.setText(String.valueOf(item.getValue()));

        timeMarkText.setText(item.getTimeMark());
        timeText.setText(item.getTime());
        timeLogText.setText(item.getLogTime());

        return rowView;

    }

    private String getStatusText(int status) {
        String res = "";
        if( (status & 0x01) == 0x01 )
            res += "Ошибка (нет значения);";
        if( (status & 0x02) == 0x02 )
            res += "Недостоверное значение;";
        if( (status & 0x04) == 0x04 )
            res += "Неточное, неполное или приближённое значение;";
        if( (status & 0x10) == 0x10 )
            res += "Вычисленное значение;";
        if( (status & 0x20) == 0x20 )
            res += "Ручной ввод;";
        if( (status & 0x100) == 0x100 )
            res += "Линейный пересчёт на этапе приема в БД;";

        return res;
    }
}
