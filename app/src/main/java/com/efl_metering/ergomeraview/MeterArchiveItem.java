package com.efl_metering.ergomeraview;

/**
 * Created by kipserver on 13.04.2016.
 */
public class MeterArchiveItem {
    double value;
    int paramId;
    int kind;
    int range; //archive kind: 1 - hour, 2 - day, 3 - month
    String timeMark;
    String time;
    int tarif;
    String logTime;
    int status;

    public String getTime() {
        return time;
    }

    public double getValue() {
        return value;
    }

    public int getKind() {
        return kind;
    }

    public int getParamId() {
        return paramId;
    }

    public int getRange() {
        return range;
    }

    public int getStatus() {
        return status;
    }

    public int getTarif() {
        return tarif;
    }

    public String getLogTime() {
        return logTime;
    }

    public String getTimeMark() {
        return timeMark;
    }

    public MeterArchiveItem setTime(String time) {
        this.time = time;
        return this;
    }

    public MeterArchiveItem setKind(int kind) {
        this.kind = kind;
        return this;
    }

    public MeterArchiveItem setLogTime(String logTime) {
        this.logTime = logTime;
        return  this;
    }

    public MeterArchiveItem setParamId(int paramId) {
        this.paramId = paramId;
        return this;
    }

    public MeterArchiveItem setRange(int range) {
        this.range = range;
        return this;
    }

    public MeterArchiveItem setStatus(int status) {
        this.status = status;
        return this;
    }

    public MeterArchiveItem setTarif(int tarif) {
        this.tarif = tarif;
        return this;
    }

    public MeterArchiveItem setTimeMark(String timeMark) {
        this.timeMark = timeMark;
        return this;
    }

    public MeterArchiveItem setValue(double value) {
        this.value = value;
        return this;
    }
}

