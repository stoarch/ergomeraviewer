package com.efl_metering.ergomeraview;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.efl_metering.ergomeraview.dummy.DummyContent;
import com.efl_metering.ergomeraview.dummy.DummyContent.DummyItem;

import org.apache.http.client.methods.HttpGet;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Intent.getIntent;
import static android.content.Intent.getIntentOld;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SubMeterFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_METER_ID = "MeterId";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private int meterId;
    private Context mContext;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */

    public SubMeterFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    List<String> meters = Arrays.asList("Загрузка счётчиков...");

    private static final String LOAD_ACTION = "com.efl_metering.ergomeraview";
    private ProgressDialog progress;

    private MeterListResult metersResult;
    private MeterListJSONParser resultParser;

    private RecyclerView listView;
    private MeterInfo meter;

    public interface OnMetersLoaded {
        void onLoaded();
    }

    private OnMetersLoaded metersLoadedListener;

    public void setOnMetersLoaded( OnMetersLoaded listener )
    {
        metersLoadedListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submeter_list, container, false);

        mContext = getActivity().getBaseContext();

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MySubMeterRecyclerViewAdapter(meters, mListener));

            listView = recyclerView;
            ItemClickSupport itemClickSupport = ItemClickSupport.addTo(recyclerView)
                    .setOnItemClickListener(
                            new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked (RecyclerView recyclerView,int position, View v)
                                {
                                    ArrayList<MeterInfo> meterList = metersResult.getMeters();

                                    if (meterList.size() > 0) {
                                        MeterInfo meter = meterList.get(position);
                                        Intent subNodesIntent = new Intent(mContext, SubnodesContainer.class);
                                        subNodesIntent.putExtra("meter", meter);
                                        startActivity(subNodesIntent);
                                    } else {
                                        Toast.makeText(mContext, "No meters found", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

            meterId= getArguments().getInt("meterId");
            meter = (MeterInfo)getArguments().getSerializable("meter");

            GetSubChilds(meterId);
        }
        return view;
    }

    private void GetSubChilds(final int meterId) {

        new Thread(){
            public void run(){
                //Get data//
                try {
                    String url = "http://92.47.27.114:9291/info/devices/roots/" + Integer.toString( meterId ) + "/childs";
                    HttpGet searchRequest = new HttpGet(new URI(url));

                    RestTask task = new RestTask(mContext.getApplicationContext(), LOAD_ACTION);
                    String response = task.executeSync(mContext, url);

                    resultParser = new MeterListJSONParser();

                    resultParser.parseMeterList(response);
                    metersResult = resultParser.getMeterList();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            meters = resultParser.getMetersCaption();
                            if( meters.size() == 0 )
                                meters.add("Нет подсчётчиков");

                            listView.setAdapter(new MySubMeterRecyclerViewAdapter(meters, mListener));

                            if(metersLoadedListener != null)
                            {
                                metersLoadedListener.onLoaded();
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String response = intent.getStringExtra(RestTask.HTTP_RESPONSE);
            resultParser.parseMeterList(response);
            metersResult = resultParser.getMeterList();
            meters = resultParser.getMetersCaption();

            listView.setAdapter(new MySubMeterRecyclerViewAdapter(meters, mListener));

            if( metersLoadedListener != null )
                metersLoadedListener.onLoaded();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(String item);
    }
}
